package com.example.cotizacion;

import java.io.Serializable;
import java.util.Random;

public class cotizacion implements Serializable {
    private int folio;
    private String descripcion;
    private float valorAuto;
    private float porEnganche;
    private int plazos;

    public cotizacion(int folio, String descripcion, float valorAuto, float porEnganche, int plazos) {
        this.folio = folio;
        this.descripcion = descripcion;
        this.valorAuto = valorAuto;
        this.porEnganche = porEnganche;
        this.plazos = plazos;
    }

    public cotizacion(){
        folio=this.generarFolio();
    }
//Folio
    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }
//Descripcion
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
//Valor
    public float getValorAuto() {
        return valorAuto;
    }

    public void setValorAuto(float valorAuto) {
        this.valorAuto = valorAuto;
    }
//Enganche
    public float getPorEnganche() {
        return porEnganche;
    }

    public void setPorEnganche(float porEnganche) {
        this.porEnganche = porEnganche;
    }
//Plazos
    public int getPlazos() {
        return plazos;
    }

    public void setPlazos(int plazos) {
        this.plazos = plazos;
    }
//Para sacar el folio
    public int generarFolio(){
        return Math.abs((int)new Random().nextInt()%1000);
    }
//calcular pago inicial
    public float calcularPagoInicial(){
    return this.valorAuto * (this.porEnganche)/100;
    }
//calcular pago mensual
    public float calcularPagoMensual(){

        return (this.valorAuto - this.calcularPagoInicial())/this.plazos;
    }

}
