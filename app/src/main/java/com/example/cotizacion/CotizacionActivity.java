
package com.example.cotizacion;

        import androidx.appcompat.app.AppCompatActivity;

        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.RadioButton;
        import android.widget.TextView;
        import android.widget.Toast;

public class CotizacionActivity extends AppCompatActivity {

    private cotizacion cotizacion=new cotizacion();
    private TextView txtcliente;
    private TextView txtfolio;
    private Button btnregresar;
    private Button btncalcular;
    private Button btnlimpiar;
    private EditText txtdescripcion;
    private EditText txtvalorauto;
    private EditText txtpagoinicial;
    private RadioButton rdb12;
    private RadioButton rdb24;
    private RadioButton rdb36;
    private RadioButton rdb48;
    private TextView lblpagomensual;
    private TextView lblpagoinicial;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        //
        txtcliente=(TextView) findViewById(R.id.txtcliente);

        txtfolio=(TextView)findViewById(R.id.txtfolio);
        //
        btnregresar=(Button) findViewById(R.id.btnregresar);
        btncalcular=(Button) findViewById(R.id.btncalcular);
        btnlimpiar=(Button) findViewById(R.id.btnlimpiar) ;
        //
        txtdescripcion=(EditText) findViewById(R.id.txtdescripcion);
        txtvalorauto=(EditText) findViewById(R.id.txtvalorauto);
        txtpagoinicial=(EditText) findViewById(R.id.txtpagoinicial);
        //
        rdb12=(RadioButton) findViewById(R.id.rdb12);
        rdb24=(RadioButton) findViewById(R.id.rdb24);
        rdb36=(RadioButton) findViewById(R.id.rdb36);
        rdb48=(RadioButton) findViewById(R.id.rdb48);
        //
        lblpagomensual=(TextView) findViewById(R.id.lblpagomensual);
        lblpagoinicial=(TextView) findViewById(R.id.lblpagoinicial);

        Bundle datos=getIntent().getExtras();
        txtcliente.setText(datos.getString("cliente"));

        cotizacion=(cotizacion) datos.getSerializable("cotizacion");
        txtfolio.setText(String.valueOf("folio:"+cotizacion.getFolio()));


        //boton pa ajuera
       btnregresar.setOnClickListener(new View.OnClickListener() {
           @Override
            public void onClick(View v) {
                finish();
          }
        });
        //boton para calcular chido aca perron
        btncalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtdescripcion.getText().toString().matches("")||
                        txtvalorauto.getText().toString().matches("")||
                        txtpagoinicial.getText().toString().matches("")){
                    Toast.makeText(CotizacionActivity.this,"falto algo",Toast.LENGTH_SHORT).show();

                    txtdescripcion.requestFocus();
                }else{
                    int plazos = 0;
                    float enganche=0;
                    float pagomensual=0.0f;

                    if(rdb12.isChecked()) {plazos= (int) 12f;}
                    if(rdb24.isChecked()) {plazos= (int) 24f;}
                    if(rdb36.isChecked()) {plazos= (int) 36f;}
                    if(rdb48.isChecked()) {plazos= (int) 48f;}



                    cotizacion.setDescripcion(txtdescripcion.getText().toString());
                    cotizacion.setValorAuto(Float.parseFloat(txtvalorauto.getText().toString()));
                    cotizacion.setPorEnganche(Float.parseFloat(txtpagoinicial.getText().toString()));
                    cotizacion.setPlazos(plazos);


                    enganche=cotizacion.calcularPagoInicial();
                    pagomensual=cotizacion.calcularPagoMensual();

                    lblpagomensual.setText("Pago Mensual$  " + String.valueOf(pagomensual));
                    lblpagoinicial.setText("Pago Inicial$ " +String.valueOf(enganche));


                }


            }
        });

        //boton pa dejar limpio
        btnlimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtdescripcion.setText("");
                txtpagoinicial.setText("");
                txtvalorauto.setText("");
                lblpagomensual.setText("");
                lblpagoinicial.setText("");

            }
        });

    }
}




